Rails.application.routes.draw do
  post 'auth/login' => 'user_token#create'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  #method  url       controller&function
  get     'user' => 'user#getAll'
  post    'user' => 'user#register'
  get     'user/:id' => 'user#showUser' 

  get     'userActive' => 'user#userActive'


  #post
  post    'post' => 'post#createPost'
  get     'post' => 'post#getAllPost'

  #group
  post    'group' => 'group#create_group'
  post    'group/:id' => 'group#join_group'

end
