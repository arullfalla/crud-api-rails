class User < ApplicationRecord
    has_many :posts
    has_and_belongs_to_many :group
    has_secure_password
    validates :username, uniqueness:true
    validates :email, uniqueness:true 
end
