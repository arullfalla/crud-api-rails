class UserController < ApplicationController
    before_action :authenticate_user, only: [ :showUser, :userActive ]

    def getAll #untuk menampilkan semua data user
        get = User.all
        render json: {
            success: true,
            data: get,
            status: 200
        }
    end

    def userActive
        user = current_user
        render json: {
            success: true,
            data: user,
            status: 200
        }
        puts user
    end

    def register #untuk menambah data user
        user = User.create(user_param)
        render json: {
            success: true,
            data: user,
            status: 200
        }
    end

    def showUser #untuk menampilkan user dengan parameter id
        user = User.find(params[:id])
        render json: {
            succcess: true,
            data: user,
            status: 200
        }
    end
   
    private

    def user_param
        params.permit(:username, :email, :password)
    end
end
