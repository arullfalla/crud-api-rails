class PostController < ApplicationController
    before_action :authenticate_user, only:[ :create]

    def createPost
        @post = current_user.posts.create({
            :title => params[:title],
            :description => params[:description]
        })
        render json: {
            success: true,
            data: @post,
            status: 200
        }
    end
    def getAllPost
        post = Post.all
        render json: {
            success: true,
            data: post,
            status: 200
        }
    end
end
