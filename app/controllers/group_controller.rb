class GroupController < ApplicationController
    before_action :authenticate_user, only: [:join_group]
    
    def create_group
        group = Group.create(params.permit(:name, :group_type)) 
        render json: {
            success: true,
            data: group,
            status: 200
        }
    end

    def join_group
        group = Group.find(params[:id])
        user = current_user.id
        user_already_exist = group.user.select {|var| var.id == user}.present?
        if user_already_exist
            render json: {
            success: false,
            data: user,
            status: 400
        }else
            current_user.group << group
            render json: {
                success: true,
                data: group,
                status: 200
            }
        end
    end


    def cek_join_group
        group = Group.find(params[:id])
        group = group.users
        render json: {
            success: true,
            data: group,
            status: 200
        }
    end

end
